<?

$MESS ['MFT_ERROR_NAME'] = "Name must contain at least 2 characters";
$MESS ['MFT_ERROR_MESSAGE'] = "Message must contain at least 10 characters";
$MESS ['MFT_ERROR_PHONE'] = "You entered an incorrect phone number";

$MESS ['MFT_NAME'] = "Name";
$MESS ['MFT_EMAIL'] = "E-mail";
$MESS ['MFT_MESSAGE'] = "Message";
$MESS ['MFT_PHONE'] = "Phone";
$MESS ['MFT_NOTE'] = "Send Us A Note";
$MESS ['MFT_CAPTCHA'] = "CAPTCHA";
$MESS ['MFT_CAPTCHA_CODE'] = "Type the letters you see on the picture";
$MESS ['MFT_SUBMIT'] = "SUBMIT";
?>