<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="bg-color-sky-light">
    <div class="content-lg container">
        <div class="row row-space-1">
            <? $count = 1;
            $properties = array();
            foreach ($arResult["ITEMS"] as $arItem):
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <? if ($count % 3 == 1 && $count > 3): ?>
                <div class="row row-space-1">
            <? endif; ?>

                <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
                     class="col-sm-4 <? if ($count % 3 != 0): ?> sm-margin-b-2<? endif; ?>">
                    <div class="wow fadeInLeft animated" data-wow-duration=".3" data-wow-delay=".1s"
                         style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">

                        <div class="pricing">
                            <div class="margin-b-30">
                                <i class="pricing-icon <?= $arItem["DISPLAY_PROPERTIES"]['icon']['VALUE'] ?>"></i>
                                <h3><? echo $arItem["NAME"] ?>
                                    <span> - <?= $arItem["DISPLAY_PROPERTIES"]['currency']['VALUE'] ?></span> <?= $arItem["DISPLAY_PROPERTIES"]['price']['VALUE'] ?>
                                </h3>
                                <p><? echo $arItem["PREVIEW_TEXT"]; ?></p>
                            </div>
                            <ul class="list-unstyled pricing-list margin-b-50">
                                <li class="pricing-list-item"><?= $arItem["DISPLAY_PROPERTIES"]['features']['VALUE'] ?></li>
                                <li class="pricing-list-item"><?= $arItem["DISPLAY_PROPERTIES"]['products']['VALUE'] ?></li>
                                <li class="pricing-list-item"><?= $arItem["DISPLAY_PROPERTIES"]['panels']['VALUE'] ?></li>
                            </ul>
                            <a href="javascript:void(0)" id="<?= $arItem['ID'] ?>"
                               class="btn-popup-pricing btn-theme btn-theme-sm btn-default-bg text-uppercase"><?= GetMessage("MFT_PRICING_BUTTON") ?></a>
                        </div>
                    </div>
                </div>

                <? if ($count % 3 == 0 && $count != 1): ?>
                </div>
            <? endif;$count++;
                $properties[$arItem["ID"]] = $arItem['NAME'];
                ?>
            <? endforeach;?>
        </div>
    </div>

    <script>
        BX.ready(function () {
            BX.bindDelegate(document.body, 'click', {className: 'btn-popup-pricing'}, function (e) {
                var d = JSON.parse('<?=json_encode($properties)?>');
                console.log(d);
                BX.ajax({
                    url: '/local/ajax/ajax.php',
                    data: {
                        ajax: 'Y',
                        data: d,
                        index: e.target.id
                    },
                    method: 'POST',
                    onsuccess: function (data) {
                        var t = new BX.PopupWindow(
                            "pricing_popup",
                            null,
                            {
                                content: data,
                                closeIcon: {right: "20px", top: "10px"},
                                titleBar: {content: ''},
                                zIndex: 0,
                                offsetLeft: 0,
                                offsetTop: 0,
                                draggable: {restrict: false}
                            });
                        t.show();
                    }
                });
            });
        });
    </script>



