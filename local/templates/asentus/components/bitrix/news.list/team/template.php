<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="bg-color-sky-light">
    <div class="content-lg container">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "EDIT_MODE" => "html",
                "PATH" => SITE_DIR . "include/team_info.php"
            )
        ); ?>
        <div class="row">
            <? $count = 1;
            foreach ($arResult["ITEMS"] as $arItem):
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                if ($count % 3 == 1 && $count > 3): ?>
                    <div class="row" style="margin-top:50px">
                <? endif; ?>
                <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="col-sm-4 sm-margin-b-50">
                    <div class="bg-color-white margin-b-20">
                        <div class="wow zoomIn" data-wow-duration=".3" data-wow-delay=".1s">
                            <? if ($arItem["PREVIEW_PICTURE"]):?>
                                <img class="img-responsive" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                     alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>">
                            <? endif; ?>
                        </div>
                    </div>
                    <h4>
                        <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
                        <?else:?>
                            <?echo $arItem["NAME"]?>
                        <?endif;?>
                        <span class="text-uppercase margin-l-20"><?= $arItem["DISPLAY_PROPERTIES"]['position']['VALUE'] ?></span>
                    </h4>
                    <p><? echo $arItem["PREVIEW_TEXT"]; ?></p>
                    <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                        <a class="link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?=GetMessage('MFT_TEAM_BUTTON')?></a>
                    <?endif;?>
                </div>
                <? if ($count % 3 == 0 && $count != 1): ?>
                </div>
            <? endif;
                $count++; ?>
            <? endforeach; ?>
        </div>
    </div>







