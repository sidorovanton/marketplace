<?
$MESS ['MFT_ERROR_NAME'] = "Имя должно содержать не менее 2 символов";
$MESS ['MFT_ERROR_MESSAGE'] = "Сообщение должно содержать не менее 10 символов";
$MESS ['MFT_ERROR_PHONE'] = "Вы ввели некорректный телефон";

$MESS ['MFT_NAME'] = "Имя";
$MESS ['MFT_EMAIL'] = "E-mail";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['MFT_PHONE'] = "Телефон";
$MESS ['MFT_NOTE'] = "Отправьте нам сообщение";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "ОТПРАВИТЬ";
?>