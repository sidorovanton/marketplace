<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetTitle($arResult["NAME"]);
$APPLICATION->SetPageProperty("description", $arResult["PREVIEW_TEXT"]); ?>
<div class="container">
    <div class="row margin-b-20">
        <div class="col-sm-6">
            <h2>
                <?= $arResult["NAME"] ?>
                <? if (is_array($arResult["DISPLAY_PROPERTIES"]["SOLUTION"])): ?>
                    <span class="text-uppercase margin-l-20">
                        <?=$arResult["DISPLAY_PROPERTIES"]["SOLUTION"]["DISPLAY_VALUE"]?>
                    </span>
                <? endif ?>
            </h2>
        </div>
    </div>
    <div class="row margin-b-20">
        <div class="col-sm-7">
            <? if (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
                <div class="sm-margin-b-50">
                    <p><? echo $arResult["DETAIL_TEXT"]; ?></p>
                </div>
            <? else: ?>
                <div class="sm-margin-b-50">
                    <p><? echo $arResult["PREVIEW_TEXT"]; ?></p>
                </div>
            <? endif ?>
        </div>
        <div class="col-sm-4 col-sm-offset-1">
            <? if (is_array($arResult["DETAIL_PICTURE"])): ?>
                <img
                    class="img-responsive"
                    src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"
                    width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>"
                    height="<?= $arResult["DETAIL_PICTURE"]["HEIGHT"] ?>"
                    alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>"
                    title="<?= $arResult["DETAIL_PICTURE"]["TITLE"] ?>"
                />
            <?elseif (is_array($arResult["PREVIEW_PICTURE"])):?>
                <img
                    class="img-responsive"
                    src="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>"
                    width="<?= $arResult["PREVIEW_PICTURE"]["WIDTH"] ?>"
                    height="<?= $arResult["PREVIEW_PICTURE"]["HEIGHT"] ?>"
                    alt="<?= $arResult["PREVIEW_PICTURE"]["ALT"] ?>"
                    title="<?= $arResult["PREVIEW_PICTURE"]["TITLE"] ?>"
                />
            <? else: ?>
                <img
                    class="img-responsive"
                    src="<?= SITE_TEMPLATE_PATH ?>/img/no-photo.png"
                />
            <? endif ?>
        </div>
    </div>
</div>