<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/about/team/(.+?)/#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "/about/team/detail.php",
	),
	array(
		"CONDITION" => "#^/products/(.+?)/#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "/products/detail.php",
	),
	array(
		"CONDITION" => "#^/services/(.+?)/#",
		"RULE" => "ID=\$1",
		"ID" => "",
		"PATH" => "/services/index.php",
	),
	array(
		"CONDITION" => "#^/about/(.+?)/#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "/about/detail.php",
	),
	array(
		"CONDITION" => "#^/services/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/products/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/products/index.php",
	),
	array(
		"CONDITION" => "#^/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
	),
);

?>