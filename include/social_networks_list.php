<ul class="list-unstyled footer-list">
	<li class="footer-list-item"><a class="footer-list-link" href="https://twitter.com/">Twitter</a></li>
	<li class="footer-list-item"><a class="footer-list-link" href="https://www.facebook.com/">Facebook</a></li>
	<li class="footer-list-item"><a class="footer-list-link" href="https://www.instagram.com/">Instagram</a></li>
	<li class="footer-list-item"><a class="footer-list-link" href="https://www.youtube.com/">YouTube</a></li>
</ul>