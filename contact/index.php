<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Contact");
$APPLICATION->SetPageProperty("description", "PAGE DESCRIPTION");
?>
<? $APPLICATION->IncludeFile(
    '/include/contact_offices.php',
    array(),
    array('MODE' => 'html')
); ?>


<? $APPLICATION->IncludeFile(
    '/include/map_section.php',
    array(),
    array('MODE' => 'html')
); ?>

<? $APPLICATION->IncludeFile(
    '/include/promo_section.php',
    array(),
    array('MODE' => 'html')
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>