<? require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php');
global $APPLICATION;
$APPLICATION->IncludeComponent(
    "bitrix:feedback",
    "footer",
    array(
        //"PRICING_INDEX" => $_REQUEST['index'],
        //"PRICING_LIST" => $_REQUEST['data'],
        "USE_CAPTCHA" => "N",
        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
        "EMAIL_TO" => "sidorovanton95@list.ru",
        "REQUIRED_FIELDS" => array(
            0 => "NAME",
            1 => "EMAIL",
            2 => "MESSAGE",
            3 => "PHONE",
        ),
        "EVENT_MESSAGE_ID" => array(
            0 => "8",
        ),
        "COMPONENT_TEMPLATE" => "footer"
    ),
    false
); ?>