<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<? if ($_REQUEST['ajax'] != 'Y'): ?>
<div class="col-sm-5 sm-margin-b-30">
    <? endif; ?>
    <div class="mf-error-text<? if ($_REQUEST['ajax'] == 'Y'):echo'-popup';endif; ?>"></div>
    <h2 class="color-white"><?= GetMessage("MFT_NOTE") ?></h2>
    <form class="user_form" name="user_form" action="<? if ($_REQUEST['ajax'] == 'Y') {echo '/';} else {echo POST_FORM_ACTION_URI;} ?>" method="POST">
        <?= bitrix_sessid_post() ?>

        <input name="user_name" value="<?= $arResult["AUTHOR_NAME"] ?>" type="text"
               class="form-control footer-input margin-b-20" placeholder="<?= GetMessage("MFT_NAME") ?>" required>
        <input name="user_email" type="email" value="<?= $arResult["AUTHOR_EMAIL"] ?>"
               class="form-control footer-input margin-b-20" placeholder="<?= GetMessage("MFT_EMAIL") ?>" required>
        <input type="text" name="user_phone" value="<?= $arResult["AUTHOR_PHONE"] ?>"
               class="form-control footer-input margin-b-20" placeholder="<?= GetMessage("MFT_PHONE") ?>" required>
        <? if ($_REQUEST['ajax'] == 'Y'): ?>
            <select name="user_pricing" class="form-control margin-b-20 footer-input">
                <? if ($arResult['PRICING_LIST']&&$arResult['PRICING_INDEX']): ?>
                    <? foreach ($arResult['PRICING_LIST'] as $key => $item): ?>
                        <option class="footer-input" <? if ($key == $arResult['PRICING_INDEX']) echo ' selected' ?>>
                            <?= $item ?>
                        </option>
                    <? endforeach; ?>
                <? else: ?>
                    <option>''</option>
                <?endif;?>
            </select>
        <? endif; ?>

        <textarea name="MESSAGE" class="form-control footer-input margin-b-30" rows="6"
                  placeholder="<?= GetMessage("MFT_MESSAGE") ?>"
                  required style="resize: none"><?= $arResult["MESSAGE"] ?></textarea>

        <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
        <input type="submit" class="btn-form-submit btn-theme btn-theme-sm btn-base-bg text-uppercase" name="submit"
               value="<?= GetMessage("MFT_SUBMIT") ?>">
    </form>
    <? if ($_REQUEST['ajax'] != 'Y'): ?>
</div>
<? endif;
$param['name']= GetMessage("MFT_ERROR_NAME");
$param['phone']= GetMessage("MFT_ERROR_PHONE");
$param['message']= GetMessage("MFT_ERROR_MESSAGE");
?>
<script>
    $(document).ready(function(){
        popupInit(<?echo json_encode($param); ?>);
    });
</script>

<? if ($_REQUEST['ajax'] == 'Y'): ?>
    <script>
        $(".user_form").submit(function() {
            if(!validate(this)) {return false} else {return true};
        });

        var ul = document.getElementsByClassName('mf-error-text-popup');
        var validate = function(el) {
            $(ul).text('');
            $count= 0;
            if(el.user_name.value.length<=2){
                $(ul).append("<p><?= GetMessage("MFT_ERROR_NAME") ?></p>");
                $count++;
            }

            if(!/([\+]+)*[0-9]{9,12}/.test(el.user_phone.value)){
                $(ul).append("<p><?= GetMessage("MFT_ERROR_PHONE") ?></p>");
                $count++;
            }

            if(el.MESSAGE.value.length<10){
                $(ul).append("<p><?= GetMessage("MFT_ERROR_MESSAGE") ?></p>");
                $count++;
            }

            if($count>0) {return false} else {return true};
        };
    </script>
<?endif;?>


